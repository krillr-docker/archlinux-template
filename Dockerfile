FROM registry.gitlab.com/krillr-docker/archlinux-base
MAINTAINER Aaron Krill <aaron@krillr.com>

### Grab the latest mirror list
RUN curl -o /etc/pacman.d/mirrorlist "https://www.archlinux.org/mirrorlist/?country=US&protocol=https&ip_version=6&use_mirror_status=on" && \
  sed -i 's/^#//' /etc/pacman.d/mirrorlist

CMD bash
